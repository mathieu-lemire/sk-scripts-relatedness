
################
ARGS<-commandArgs(TRUE)

genome<- ARGS[1]
prefix<- ARGS[2] 

# othersise plot in pdf 
usex11<- TRUE 
if( length( ARGS )== 3 ){
 usex11 <- as.logical( ARGS[3] )
}

library(utils)
library( stats )


g<- read.table(genome, head=T )
o<- order(g$Kinship)
g<- g[o,] 



if( usex11 ){
 x11( width=14, height=7 ) 
} else {
 pdf( file= paste0( prefix ,".relatedness.pdf" ) , width=14, height=7 )
}


par( mfrow=c(1,2) )

COL<- as.integer( g$InfType )

plot(  g$Kinship, main=genome, col=COL  )
abline( h=1/( 2^(0:4) ), lty=3 )
legend( x="topleft", legend=paste0( "KING ", levels( g$InfType )), col=1:length( g$InfType ) , pch=19 )


plot(  g$Kinship , ylim=c( 1/2^6, 1/2^2 ) , yaxt="n" , col=COL  )
axis( 2, at=seq( 0,.25, .02 ) )
abline( h=1/( 2^(0:4) ), lty=1 )
axis(4, at=1/( 2^(0:4) ) )
abline( h=seq( 0,.25, .02 )  , lty=3 )

#cat( "#################################\nChoose treshold that separates cousins from half-sibs, then execute function setKINSHIPThreshold\n")
#cat("E.g.\nthr<-0.0625\nsetKINSHIPThreshold(thr, ibdclustermeans=c(0.125, 0.25, 0.5 )  )\n" )
#cat("setKINSHIPThreshold(thr, ibdclustermeans=c( 0.25 , 0.50 )  )\n" )
#cat( "#################################\n")




if( !usex11 ){
  dev.off()
}



setKINSHIPThreshold<-function(thresh, ibdclustermeans=c(0.125, 0.25, 0.5 ), usex11=TRUE  ){

 g.rel<- g[ g$Kinship> thresh, ] 
 clus<- kmeans( g.rel$Kinship, centers=ibdclustermeans  )
 G<- length( ibdclustermeans )
 for( i in 1:G ){
  sel<- clus$cluster == i
  g.rel$EZ[sel]<- ibdclustermeans[ i ]
 }
  g.rel$THRESHOLD<- thresh 
  write.table( g.rel, paste0( prefix ,".relatedness.txt" ), col=T, row=F, quote=F )

if( TRUE ){
 if( usex11 ){
   x11( width=7, height=7 ) 
 } else {
    pdf( file= paste0( prefix ,".relatedness.pdf" )  ,  width=7, height=7 ) 
 }
  plot(  g.rel$Kinship , col=clus$cluster+1 )
  abline( h=1/( 2^(0:4) ), lty=3 )
  axis(4, at=1/( 2^(0:4) ) )
  if( usex11 ){
   dev.copy2pdf( file= paste0( prefix ,".relatedness.pdf" )  )
  } else {
   dev.off()
  }
}
}











#############################################################################
#   Copyright 2019 Mathieu Lemire
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#############################################################################