#!/bin/bash



module load plink/1.90b3x
module load R/3.5.1
module load king/2.1.6

scriptdir=/hpf/projects/arnold/users/mlemire/scripts/relatedness

# where the plink file reside 
dir=$1
# prefix of the bfiles
prefix=$2
qcdir=$3 
#  this can be empty
usex11=$4 

tmpfile=_$$_tmp_ 

# ld pruning 

touch ${tmpfile}.exclude
if [ -f $qcdir/${prefix}.exclude.txt ]; then 
 cat  $qcdir/${prefix}.exclude.txt  > ${tmpfile}.exclude
fi
awk '$1>22 {print $2}'  $dir/${prefix}.bim >> ${tmpfile}.exclude

touch ${tmpfile}.remove 
if [ -f $qcdir/controls.remove.txt ]; then
 cat  $qcdir/controls.remove.txt >>   ${tmpfile}.remove
fi
if [ -f $qcdir/${prefix}.controls.txt ]; then 
 cat  $qcdir/${prefix}.controls.txt >>   ${tmpfile}.remove 
fi
if [ -f $qcdir/${prefix}.remove.txt ]; then 
 cat  $qcdir/${prefix}.remove.txt | awk '{print $1,$2}'  >>   ${tmpfile}.remove 
fi
if [ -f $qcdir/${prefix}.1kgpca.outliers.txt ]; then 
 cat  $qcdir/${prefix}.1kgpca.outliers.txt >>   ${tmpfile}.remove 
fi


awk '{print $1,$2}'  $dir/${prefix}.fam  > ${tmpfile}.keep

\plink --memory 8000 --bfile $dir/$prefix --exclude  ${tmpfile}.exclude --remove  ${tmpfile}.remove   --indep-pairwise 1000 500 0.20 --maf 0.25 --out ${tmpfile} --keep ${tmpfile}.keep

kingprefix=`echo $prefix | rev | cut -f 1 -d'/' | rev` 

\plink  --memory 8000 --bfile $dir/$prefix --extract ${tmpfile}.prune.in --remove  ${tmpfile}.remove --make-bed --out ${prefix}_KING 

king -b ${prefix}_KING.bed  --related --degree 4 --prefix ${prefix}_KING 

R --no-save --args ${prefix}_KING.kin0  ${prefix} $usex11 < $scriptdir/relate.r 

 echo ... done

\rm ${tmpfile}*


#############################################################################
#   Copyright 2019 Mathieu Lemire
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#############################################################################
